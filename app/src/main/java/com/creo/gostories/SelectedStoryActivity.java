package com.creo.gostories;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Nishant Thite on 28-07-2015.
 */
public class SelectedStoryActivity extends Activity // implements View.OnClickListener
{
    static boolean syncUpdate = true;
    static AudioPlayerTest audioPlayerTest;
    TextView storyname,bookname,duration,director,authorname,artistname;
    ImageView authoreimage,artistimage,authoreimageinfo,artistimageinfo;
    //static AudioPlayerView audioPlayerViewObj = null;
    Button stopAudioBtn;
    Button playPauseAudioBtn,preview,story;
    String clickedId;
    String idType;
    final String preview_story = "preview_story";
    final String story_url = "story_url";
    static String  previewstory = null;
    static String  storyurl = null;
    LinearLayout llseekbar,llfortime;
    RelativeLayout playerLayout;
    private AlertDialog alertDialog;
    ArrayList purchasedStoryList;
    private int WalletBalance  ;
    Boolean success_response = false;
    Boolean error_response = false;
    Boolean parsing_error = false;
    Boolean in_Promocode = false;
    Boolean in_BalenceError = false;
    Boolean in_BalanceUpdate = false;
    Boolean storyIDOnServer = false;
    Boolean in_isPurchasedStory = false;
    Boolean checkOnServer = true;
    Boolean buyStory = false;
    Boolean purchasedStory = false;
    Boolean stopProgressDialog = false;
    Boolean playStory = false;
    static String storyPurchaseResponce ;
    static String goStoriesPoints = " GoStoriesPoints" ;
    static String storyCost ;
   // SharedPreferences mySharedPreferences;
    final static String PURCHASED_STORIES = "PURCHASED_STORIES";
    static int ONE_STORY_COST = 100;
    Context context = this;
    static String storyID = "";
    ProgressDialog pd;
    static String No = "No";
    String purchasedStoryListName = "purchasedStoryListAsString";
 //   String[] purchasedStoryListArr = {};
    ArrayList<String> purchasedStoryListArr = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.selected_story);

        purchasedStoryList = new ArrayList();
     //  mySharedPreferences = getSharedPreferences(UserRegisterationActivity.MY_PREFS_NAME, MODE_PRIVATE);

        EarnPointsActivity.pref=getApplication().getSharedPreferences(UserRegisterationActivity.MY_PREFS_NAME, MODE_PRIVATE);
        EarnPointsActivity.total_point = EarnPointsActivity.pref.getString(UserRegisterationActivity.total_point, "");
//        WalletBalance = Integer.parseInt(UserRegisterationActivity.totalPoint);
        EarnPointsActivity.email_ID = EarnPointsActivity.pref.getString(UserRegisterationActivity.email, "");



    //    Log.d(HomeActivity.LogTag, "email in singlestory" + PromoCodeActivity.email_ID);
    //    Log.d(HomeActivity.LogTag, "toal_point in singlestory" + PromoCodeActivity.total_point);
//        AdView mAdView = (AdView) findViewById(R.id.adView);
//        AdRequest adRequest = new AdRequest.Builder().build();
//        mAdView.loadAd(adRequest);

        stopAudioBtn = (Button) findViewById(R.id.stopAudioBtnId);
        storyname = (TextView) findViewById(R.id.storyname);
        bookname = (TextView) findViewById(R.id.bookname);
        duration = (TextView) findViewById(R.id.duration);
        director = (TextView) findViewById(R.id.director);
        authorname = (TextView) findViewById(R.id.authorname);
        artistname = (TextView) findViewById(R.id.artistname);
        authoreimage = (ImageView) findViewById(R.id.authorimage);
        artistimage = (ImageView) findViewById(R.id.artistimage);
        authoreimageinfo = (ImageView) findViewById(R.id.authorimageinfo);
        artistimageinfo = (ImageView) findViewById(R.id.artistimageinfo);
        preview = (Button) findViewById(R.id.preview);
        story = (Button) findViewById(R.id.story);
//        llseekbar = (LinearLayout) findViewById(R.id.audioSeekBarId);
//        llfortime = (LinearLayout) findViewById(R.id.playedTextViewId);
        playerLayout = (RelativeLayout) findViewById(R.id.playerLayout);
        basicSetupForAudioView();
      //  invisible();
        Intent in = getIntent();
        final StoryListModel StoryListobj = (StoryListModel)in.getSerializableExtra("StoryList");

         storyID = StoryListobj.getStoryid();


        if(Utility.isExternalStorageReadable()) {
            String str=Utility.decodeFile( "pointerStroyListJson.txt");
            // fatchMyStoryListJson("pointerStroyListJson.txt", clickedId, IdType);
            try {
                parseMyStoryListJson(str,storyID);
            } catch (JSONException error117) {
                error117.printStackTrace();
                Log.e(HomeActivity.LogTag, "error117" + error117);
            }
        }
        else{
            try {
                fatchMyStoryListJsonFromIM("pointerStroyListJson.txt", storyID);
            } catch (IOException error118) {
                error118.printStackTrace();
                Log.e(HomeActivity.LogTag, "error118" + error118);
            }
        }
        if(PlayAudioService.mp == null) {
            audioPlayerTest.makeAudioUIInvisible();
        }

        preview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectivityManager connMgr = (ConnectivityManager)
                        getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
                if (networkInfo != null && networkInfo.isConnected()) {
                    stopService();
                    audioPlayerTest.SongUrl = previewstory;
                    audioPlayerTest.playOrPauseBtn.setText(audioPlayerTest.pauseBtnTitle);
                    audioPlayerTest.playOrPauseBtn.setBackgroundResource(R.drawable.pause);
                  //  visible();
                    audioPlayerTest.playAudioFile();
        //            Log.d(PlayAudioService.LOG_ID, "........PLAY clicked in preview......");
                }
                else {
                    String errMsg = "";
                    if (networkInfo != null){
                        errMsg = "\n\n ExtraInfo: " + networkInfo.getExtraInfo() + ". Reason: " + networkInfo.getReason()+"\n\n";
                    }else{
                        errMsg = "\n\n No connection.. \n\n";

                        Toast.makeText(getApplicationContext(), "Please check internet connection", Toast.LENGTH_LONG).show();
                    }
                }
                }
        });

        story.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkSelectedStoryAccess();
            }
        });

        stopAudioBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // PlayAudioService.bufferPercent = 0;

               // stopService();
                audioPlayerTest.stopAudioPlayer();
                audioPlayerTest.makeAudioUIInvisible();
            }
        });

        playPauseAudioBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            //    audioPlayerViewObj.playOrPauseAudioPlayer();
                audioPlayerTest.playOrPauseAudioPlayer();
        //        Log.d(PlayAudioService.LOG_ID,"........PLAY clicked in play or pause......");
            }
        });

//        Toast.makeText(getApplicationContext(),
//                      "Clicked on : " + StoryListobj.getAuthorname(), Toast.LENGTH_LONG).show();

  /*      storyname.setText(StoryListobj.getStoryname());
        bookname.setText(StoryListobj.getBookname());
        duration.setText(StoryListobj.getTimeduration());
        director.setText(StoryListobj.getDirector());

        authorname.setText(StoryListobj.getAuthorname());
        artistname.setText(StoryListobj.getArtistname());

        final String authorId = StoryListobj.getAuthoreid();
        final String artistid = StoryListobj.getArtistid();
        String authornewid="a"+authorId;
        String artistnewid="a"+artistid;
        Context context = authoreimage.getContext();
        Context context1 = artistimage.getContext();
        // int id = context.getResources().getIdentifier(newid, "drawable", context.getPackageName());
        int id = Utility.getImageById(authornewid, context);
        authoreimage.setImageResource(id);
        int id1 = Utility.getImageById(artistnewid, context1);
        artistimage.setImageResource(id1);
*/

        final String authorId = StoryListobj.getAuthoreid();
        final String artistid = StoryListobj.getArtistid();
        View.OnClickListener infoOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(v.getId() == R.id.authorimageinfo){
                    clickedId =authorId;
                    idType = StoryListActivity.author_point;
                }else if(v.getId() == R.id.artistimageinfo){
                    clickedId =artistid;
                    idType = StoryListActivity.voice_artist;
                }
                Intent intent = new Intent(SelectedStoryActivity.this,InfoActivity.class);
                intent.putExtra("clickedId", clickedId);
                intent.putExtra("IdType", idType);
                startActivity(intent);
            }
        };

        authoreimageinfo.setOnClickListener(infoOnClickListener);
        artistimageinfo.setOnClickListener(infoOnClickListener);

        basicSetupForAudioView();
    }




    public void saveStoryList(ArrayList<String> array, String arrayName) {
      //  SharedPreferences prefs = mContext.getSharedPreferences("preferencename", 0);
        SharedPreferences.Editor editor = getSharedPreferences(UserRegisterationActivity.MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putInt(arrayName +"_size", array.size());
        for(int i=0;i<array.size();i++)
            editor.putString(arrayName + "_" + i, array.get(i));
    //    Log.d(HomeActivity.LogTag, "purchasedStoryListArr in save " + array);
         editor.commit();
    }

    public ArrayList<String> loadStoryList(String arrayName, Context mContext) {

        EarnPointsActivity.pref=getApplication().getSharedPreferences(UserRegisterationActivity.MY_PREFS_NAME, MODE_PRIVATE);
        int size = EarnPointsActivity.pref.getInt(arrayName + "_size", 0);
      //  String purchasedStoryListArr[] = new String[size];
        purchasedStoryListArr.clear();

        for(int i=0;i<size;i++) {
            String value ;
            value = EarnPointsActivity.pref.getString(arrayName + "_" + i, null);
            purchasedStoryListArr.add(value);
    //        Log.d(HomeActivity.LogTag, "purchasedStoryListArr in load " + purchasedStoryListArr);
        }
        return purchasedStoryListArr;
    }

    private void stopService() {
        audioPlayerTest.stopAudioPlayer();
    }
    void checkSelectedStoryAccess(){
       // updateBalance();
        if (purchasedStoryListArr != null){
            purchasedStoryListArr = loadStoryList(purchasedStoryListName, context);
    //        Log.d(HomeActivity.LogTag, "purchasedStoryListArr in checkSelectedStoryAccess " + purchasedStoryListArr);
            if(purchasedStoryListArr.contains(storyID))
            {
                playStory();
            }
            else if(checkOnServer)
            {
                Thread thread = new Thread(new Runnable(){
                    @Override
                    public void run() {
                        loadDataFromServer();
                    }
                });

                thread.start();
    //            Log.d(HomeActivity.LogTag, "storyIDOnServer 3" + storyIDOnServer);
     //           Log.d(HomeActivity.LogTag, "purchasedStoryListArr in checkSelectedStoryAccess in stoty id check " + storyIDOnServer);
                HashMap<String, String> details = new HashMap<String, String>();
                details.put(Utility.TAG_EMAIL_ID, EarnPointsActivity.email_ID);
                details.put(Utility.StoryID, storyID);
                loadAsAsyncTask(details, Utility.isPurchasedStoryURL);
     //           Log.d(HomeActivity.LogTag, "storyIDOnServer 2" + storyIDOnServer);
            }
        }
    }

    private void loadDataFromServer() {
        try {
            URL url = new URL("http://gostories.co.in/Sandeep/DynamicData/OneStoryCostInPoints.txt");
     //       Log.d(HomeActivity.LogTag, "\nLoding  data from server\n");

            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));

            String str;
            while ((str = in.readLine()) != null) {
                storyCost = str;
            }
    //        Log.d(HomeActivity.LogTag, "\nstoryCost: " + storyCost + "\n");
            // strJson = str;
            in.close();

            if(storyCost.length()>0 && storyCost != null){

                ONE_STORY_COST = Integer.parseInt(storyCost);
    //            Log.d(HomeActivity.LogTag, "\nONE_STORY_COST: " + ONE_STORY_COST + "\n");
            }

        } catch (Exception error152) {
            String errStr = "Error in load storyCost link: " + error152;
            Log.e(HomeActivity.LogTag, errStr);
        }
    }

    private void updateBalance() {

        EarnPointsActivity.prevBalanceStr = EarnPointsActivity.total_point;
    //    Log.d(HomeActivity.LogTag, "total_point " + PromoCodeActivity.prevBalanceStr);
    //    Log.d(HomeActivity.LogTag, "prevBalanceStr " + PromoCodeActivity.prevBalanceStr);
        EarnPointsActivity.PrevBalance = Integer.parseInt(EarnPointsActivity.total_point);
        int delta= ONE_STORY_COST;
        if(syncUpdate){
            delta = 0;
        }
        EarnPointsActivity.NewBalance = EarnPointsActivity.PrevBalance - delta;
        EarnPointsActivity.NewBalanceStr = String.valueOf(EarnPointsActivity.NewBalance);
    //    Log.d(HomeActivity.LogTag, "NewBalanceStr " + PromoCodeActivity.NewBalanceStr);

        HashMap<String, String> details = new HashMap<String, String>();
        details.put(Utility.TAG_EMAIL_ID, EarnPointsActivity.email_ID);
        details.put(Utility.PrevBalance, EarnPointsActivity.prevBalanceStr);
        details.put(Utility.NewBalance, EarnPointsActivity.NewBalanceStr);
        loadAsAsyncTask(details, EarnPointsActivity.targetUpdateBalanceURL);
    }

 /*   private void deductStoryCostAndUpdateBalance() {

        PromoCodeActivity.prevBalanceStr = PromoCodeActivity.total_point;
        Log.e(HomeActivity.LogTag, "total_point " + PromoCodeActivity.prevBalanceStr);
        Log.e(HomeActivity.LogTag, "prevBalanceStr " + PromoCodeActivity.prevBalanceStr);
        PromoCodeActivity.PrevBalance = Integer.parseInt(PromoCodeActivity.total_point);
        PromoCodeActivity.NewBalance = PromoCodeActivity.PrevBalance - ONE_STORY_COST;
        PromoCodeActivity.NewBalanceStr = String.valueOf(PromoCodeActivity.NewBalance);
        Log.e(HomeActivity.LogTag, "NewBalanceStr " + PromoCodeActivity.NewBalanceStr);

        HashMap<String, String> details = new HashMap<String, String>();
        details.put(Utility.TAG_EMAIL_ID, PromoCodeActivity.email_ID);
        details.put(Utility.PrevBalance, PromoCodeActivity.prevBalanceStr);
        details.put(Utility.NewBalance, PromoCodeActivity.NewBalanceStr);
        loadAsAsyncTask(details, PromoCodeActivity.targetUpdateBalanceURL);
    }*/


    void playStory(){
        stopService();
        audioPlayerTest.SongUrl = storyurl;
        audioPlayerTest.playOrPauseBtn.setText(audioPlayerTest.pauseBtnTitle);
        audioPlayerTest.playOrPauseBtn.setBackgroundResource(R.drawable.pause);
        audioPlayerTest.playAudioFile();
       // visible();
//        audioPlayerViewObj.SongUrl = storyurl;
//        if (HomeActivity.audioPlayerViewObj != null){
//            if(HomeActivity.audioPlayerViewObj.mp != null){
//                Log.d(HomeActivity.LogTag,"\n  HomeActivityaudioPlayerViewObj.mp. : "+ String.valueOf(HomeActivity.audioPlayerViewObj.mp));
//                HomeActivity.audioPlayerViewObj.stopAudioPlayer();
//                Log.d("HomeActivityaudioPlayerViewreleseObj.mp", String.valueOf(HomeActivity.audioPlayerViewObj.mp));
//                audioPlayerViewObj.playOrPauseAudioPlayer();
//                visibleSeekbar();
//            }

//            else {
//                if(audioPlayerViewObj.mp != null) {
//                    audioPlayerViewObj.stopAudioPlayer();
//                    Log.d(HomeActivity.LogTag, "\n  audioPlayerViewObj.SongUrl : " + String.valueOf(audioPlayerViewObj.SongUrl));
//                    audioPlayerViewObj.playOrPauseAudioPlayer();
//                    visibleSeekbar();
//                }
//                else{
//                    audioPlayerViewObj.playOrPauseAudioPlayer();
//                    visibleSeekbar();
//                }
            }
//        }
//    }

    public void showStoryPurchaseDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(EarnPointsActivity.congratulations);
        builder.setMessage("You have unlocked \"" + storyname.getText() + "\"." + "\n" + "Your Wallet Balence is" + EarnPointsActivity.total_point + SelectedStoryActivity.goStoriesPoints);
                builder.setPositiveButton("Play", null);
        builder.setNegativeButton("Not now", null);
        alertDialog = builder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();

        Button continuePurchaseButton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        continuePurchaseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                playStory();

            }
        });

        Button cancelPurchaseButton = alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
        cancelPurchaseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
    }


    public void showNewStoryAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("New Story.!");
        builder.setMessage("You are about to unlock new story \"" + storyname.getText()+ "\".\nIt will cost you "+ ONE_STORY_COST+ goStoriesPoints+" and" + "\n" + "Your Wallet Balence is" + EarnPointsActivity.total_point + SelectedStoryActivity.goStoriesPoints+ "\nWould you like to continue?"+"\nIf you are not sure, you can listen to preview for free before purchasing this full story.");
        builder.setPositiveButton("Yes", null);
        builder.setNegativeButton("No", null);
        alertDialog = builder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();

        Button continuePurchaseButton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        continuePurchaseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                purchaseCurrentStory();
            }
        });

        Button cancelPurchaseButton = alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
        cancelPurchaseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
    }

    void checkWalletBalance(){
        // sync WalletBalance with server
      //  ONE_STORY_COST = 100;
        EarnPointsActivity.total_point = EarnPointsActivity.pref.getString(UserRegisterationActivity.total_point, "");
        WalletBalance = Integer.parseInt(EarnPointsActivity.total_point);
        Log.d(HomeActivity.LogTag, "WalletBalance  " + WalletBalance);
        if(ONE_STORY_COST > WalletBalance){
            showInsufficientBalanceAlertDialog();
        }else {
            showNewStoryAlertDialog();
        }
    }

    void purchaseCurrentStory(){
    //    deductStoryCostAndUpdateBalance();
        syncUpdate = false;
        buyStory = true;
        updateBalance();
//        WalletBalance = WalletBalance - ONE_STORY_COST;
//        purchasedStoryListArr.add(storyID);
//        Log.d(HomeActivity.LogTag, "purchasedStoryListArr in purchase " + purchasedStoryListArr);
//        saveArray(purchasedStoryListArr, purchasedStoryListName);
      //  purchasedStoryList.add(storyID);
      //  playStory();
    }

    @Override
    public void onResume(){
        super.onResume();
        basicSetupForAudioView();
        if(PlayAudioService.mp == null) {
            audioPlayerTest.makeAudioUIInvisible();
        }
        else if(PlayAudioService.mp != null) {
            if (!PlayAudioService.mp.isPlaying()) {
              //  Log.d(PlayAudioService.LOG_ID, "........check mp......" + PlayAudioService.mp);
                audioPlayerTest.makeAudioUIInvisible();
              //  Log.d(PlayAudioService.LOG_ID, "........check mp2......" + PlayAudioService.mp);
            } else {
                audioPlayerTest.makeAudioUIVisible();
            }
        }
    }

    public void showInsufficientBalanceAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Insufficient balance.!");
        builder.setMessage("You do not have required balance in your wallet to unlock this story.\n" +
                "Do you want to earn free points?");
        builder.setPositiveButton("Yes", null);
        builder.setNegativeButton("No", null);
        alertDialog = builder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();

        Button positiveButton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                openEarnPointsActivity();
            }
        });

        Button negativeButton = alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
        negativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
    }

    public void openEarnPointsActivity() {
        Intent i = new Intent(SelectedStoryActivity.this, EarnPointsActivity.class);
        startActivity(i);
       // finish();
      //  Toast.makeText(getApplicationContext(), "EarnPointsActivity should launch from here" , Toast.LENGTH_LONG).show();
    }

    void basicSetupForAudioView() {
        if (audioPlayerTest == null){
            audioPlayerTest = new AudioPlayerTest(this);
        }
        audioPlayerTest.initBroadcastReceiver();
        audioPlayerTest.registerBroadcastReceiver();
        audioPlayerTest.audioSeekBar = (SeekBar) findViewById(R.id.audioSeekBarId);
        audioPlayerTest.audioSeekBar.setOnSeekBarChangeListener(audioPlayerTest);
        audioPlayerTest.bufferTextView = (TextView) findViewById(R.id.bufferTextViewId);
        audioPlayerTest.bufferText = (TextView) findViewById(R.id.bufferTextView);
        audioPlayerTest.playTextView = (TextView) findViewById(R.id.playedTextViewId);
        audioPlayerTest.playText = (TextView) findViewById(R.id.playedTextView);
        playPauseAudioBtn = (Button) findViewById(R.id.playAudioBtnId);
        audioPlayerTest.ownerActivity = this;
        audioPlayerTest.playOrPauseBtn = playPauseAudioBtn;
        audioPlayerTest.audioPlayerLayout = (RelativeLayout) findViewById(R.id.playerLayout);
        audioPlayerTest.linearLayout = (LinearLayout) findViewById(R.id.linearLayout);
        audioPlayerTest.handleAudioViewAppearance();
        audioPlayerTest.stopPlayer = stopAudioBtn;
    }


    private void fatchMyStoryListJsonFromIM(String fileName, String storyID) throws IOException {
        try {

        //    Log.d(HomeActivity.LogTag, "\n mInternalStorageAvailable: " + "true" + "\n");
            BufferedReader bufferedReader = new BufferedReader(new FileReader(new
                    File(getFilesDir() + File.separator + fileName)));
            String read;
            StringBuilder builder = new StringBuilder("");

            while ((read = bufferedReader.readLine()) != null) {
                builder.append(read);
            }
            String jString = builder.toString();
        //    Log.d("Output", builder.toString());
            bufferedReader.close();
            parseMyStoryListJson(jString,storyID);


        }catch (Exception error116)
        {
            error116.printStackTrace();
            Log.e(HomeActivity.LogTag, "error116" + error116);
        }
    }


    private void parseMyStoryListJson(String jString,String storyID) throws JSONException {

        JSONArray jObject = new JSONArray(jString);

        //Iterate the jsonArray and print the info of JSONObjects
        for (int i = 0; i < jObject.length(); i++) {

            JSONObject jsonObject = jObject.getJSONObject(i);
            String storyId = jsonObject.optString(StoryListActivity.story_Id);

            if(storyId.equals(storyID)){
                String storyNameStr = jsonObject.optString(StoryListActivity.story_name);
                String booknameStr = jsonObject.optString(StoryListActivity.book_name);
                String directorStr = jsonObject.optString(StoryListActivity.director);
                String storyduration = jsonObject.optString(StoryListActivity.story_duration);
                previewstory = jsonObject.optString(preview_story);
                storyurl = jsonObject.optString(story_url);

                JSONObject author_pointJsonObject = jsonObject.getJSONObject(StoryListActivity.author_point);
                String authorNameStr = author_pointJsonObject.optString(StoryListActivity.author_name);
                String authorId = author_pointJsonObject.optString(StoryListActivity.author_id);

                JSONObject voice_artistJsonObject = jsonObject.getJSONObject(StoryListActivity.voice_artist);
                String artistnameStr = voice_artistJsonObject.optString(StoryListActivity.artist_name);
                String artistId = voice_artistJsonObject.optString(StoryListActivity.artist_id);

                storyname.setText(storyNameStr);
                bookname.setText(booknameStr);
                duration.setText(storyduration);
                director.setText(directorStr);
                authorname.setText(authorNameStr);
                artistname.setText(artistnameStr);

                String authornewid="a"+authorId;
                String artistnewid="a"+artistId;
                Context context = authoreimage.getContext();
                Context context1 = artistimage.getContext();
                // int id = context.getResources().getIdentifier(newid, "drawable", context.getPackageName());
                int id = Utility.getImageById(authornewid, context);
                authoreimage.setImageResource(id);
                int id1 = Utility.getImageById(artistnewid, context1);
                artistimage.setImageResource(id1);

//                audioPlayerViewObj.SongUrl = previewstory;
            //    Log.d("previewstory", previewstory);
            //    Log.d("storyurl", storyurl);
            }
        }
    }

    public void loadAsAsyncTask(HashMap<String, String> details, String targetURL){
        if (pd == null) {
            pd = new ProgressDialog(this);
            pd.setMessage("Loading ...");
            pd.setCancelable(false);
            pd.show();
        }
        try{
            ConnectivityManager connMgr = (ConnectivityManager)
                    getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()) {
                new SendHttpRequestTask(details,targetURL).execute();
            }
            else {

                String errMsg = "";
                if (networkInfo != null){
                    errMsg = "\n\n ExtraInfo: " + networkInfo.getExtraInfo() + ". Reason: " + networkInfo.getReason()+"\n\n";
                }else{
                    errMsg = "\n\n No connection.. \n\n";
                    pd.dismiss();
                    Toast.makeText(getApplicationContext(), "Please check internet connection", Toast.LENGTH_LONG).show();

                }
            //    Log.d(HomeActivity.LogTag, errMsg);


            }
        } catch (Exception error_107) {
            String errStr = "\n\n Error in loadStoryAsAsyncTask: " + error_107+"\n\n";
            Log.e(HomeActivity.LogTag,"\n  error_107. : "+ error_107);

            Log.e(HomeActivity.LogTag,"\n  error_107. : "+  errStr);
        }

    }

    private class SendHttpRequestTask extends AsyncTask<String, Void, String> {
        HashMap<String, String> newDetails;
        String targetURL;

        public SendHttpRequestTask(HashMap<String, String> details, String targetURL) {
            this.newDetails = details;
            this.targetURL = targetURL;
        }

        @Override
        protected String doInBackground(String... params) {

            try
            {
                EarnPointsActivity.resultJson = Utility.sendAsHtmlMulitipartForm(newDetails,targetURL);
                success_response = false;
                error_response = false;
                parsing_error = false;
                in_BalenceError = false;
                in_BalanceUpdate = false;
                in_isPurchasedStory = false;
                storyIDOnServer = false;
                purchasedStory = false;
                Utility.connection_error = false;

                if(targetURL.equals(EarnPointsActivity.targetUpdateBalanceURL)){
                    parseUpdateBalanceResponce();
                    in_BalanceUpdate = true;
                }

                if(targetURL.equals(Utility.isPurchasedStoryURL)){
                    parseisPurchasedStoryResponce();
                    in_isPurchasedStory = true;
                }

                if(targetURL.equals(Utility.purchaseStoryURL)){
                    parsePurchasedStoryResponce();
                    purchasedStory = true;
                }

            } catch (Exception error_301) {
                error_301.printStackTrace();
                Log.e(HomeActivity.LogTag, "\n  error_301. : " + error_301);
                parsing_error = true;
            }
            return null;
        }


        private void parsePurchasedStoryResponce() {
            try {

                JSONObject jsonObject = new JSONObject(EarnPointsActivity.resultJson);

                if (jsonObject.has(UserRegisterationActivity.success)) {

                    purchasedStoryListArr.add(storyID);
                //    Log.d(HomeActivity.LogTag, "purchasedStoryListArr in purchase " + purchasedStoryListArr);
                    saveStoryList(purchasedStoryListArr, purchasedStoryListName);
                    success_response = true;
                    playStory = true;
                }

                if (jsonObject.has(UserRegisterationActivity.error)) {

                    JSONObject errorObj = jsonObject.getJSONObject(UserRegisterationActivity.error);
                //    Log.d(HomeActivity.LogTag, "\n  Activity.error. : " + UserRegisterationActivity.error);

                    if (errorObj.has(EarnPointsActivity.errorCode)) {
                        EarnPointsActivity.errorValue = errorObj.optString(EarnPointsActivity.errorCode);
                //        Log.d(HomeActivity.LogTag, "\n  errorValue. : " + PromoCodeActivity.errorValue);
                        EarnPointsActivity.errorMessageValue = errorObj.optString(EarnPointsActivity.errorMessage);
                //        Log.d(HomeActivity.LogTag, "\n  errorMessageValue. : " + PromoCodeActivity.errorMessageValue);
                        error_response = true;
                    }
                }
            } catch (JSONException error_302) {
                error_302.printStackTrace();
                Log.e(HomeActivity.LogTag, "\n  error_302. : " + error_302);
                parsing_error = true;
            }
        }

        private void parseisPurchasedStoryResponce() {
            try
            {
                JSONObject jsonObject = new JSONObject(EarnPointsActivity.resultJson);
                if (jsonObject.has(UserRegisterationActivity.success)) {

                    JSONObject successObj = jsonObject.getJSONObject(UserRegisterationActivity.success);
                    storyPurchaseResponce = successObj.optString(Utility.isPurchased);
                //    Log.d(HomeActivity.LogTag, "storyPurchaseResponce " + storyPurchaseResponce);
                    if(storyPurchaseResponce.equals(No)){

                        storyIDOnServer = false;
                    }
                    else {
                        purchasedStoryListArr.add(storyID);
                //        Log.d(HomeActivity.LogTag, "purchasedStoryListArr in purchase " + purchasedStoryListArr);
                        saveStoryList(purchasedStoryListArr, purchasedStoryListName);
                        storyIDOnServer = true;
                    }
                    success_response = true;
                }

                if (jsonObject.has(UserRegisterationActivity.error)) {

                    JSONObject errorObj = jsonObject.getJSONObject(UserRegisterationActivity.error);
                //    Log.d(HomeActivity.LogTag, "\n  Activity.error. : " + UserRegisterationActivity.error);

                    if (errorObj.has(EarnPointsActivity.errorCode)) {
                        EarnPointsActivity.errorValue = errorObj.optString(EarnPointsActivity.errorCode);
                //        Log.d(HomeActivity.LogTag, "\n  errorValue. : " + PromoCodeActivity.errorValue);
                        EarnPointsActivity.errorMessageValue = errorObj.optString(EarnPointsActivity.errorMessage);
                //        Log.d(HomeActivity.LogTag, "\n  errorMessageValue. : " + PromoCodeActivity.errorMessageValue);
                        error_response = true;
                    }
                }
            } catch (JSONException error_302) {
                error_302.printStackTrace();
                Log.e(HomeActivity.LogTag, "\n  error_302. : " + error_302);
                parsing_error = true;
            }
        }

        private void parseUpdateBalanceResponce() {
            try {
                in_BalenceError = false;
                JSONObject jsonObject = new JSONObject(EarnPointsActivity.resultJson);

                if (jsonObject.has(UserRegisterationActivity.success)) {

                    SharedPreferences.Editor editor = getSharedPreferences(UserRegisterationActivity.MY_PREFS_NAME, MODE_PRIVATE).edit();
                    editor.putString(UserRegisterationActivity.total_point, EarnPointsActivity.NewBalanceStr);
                    editor.commit();
                    EarnPointsActivity.total_point = EarnPointsActivity.pref.getString(UserRegisterationActivity.total_point, "");
                //    Log.d(HomeActivity.LogTag, "toal_point " + PromoCodeActivity.total_point);

                    success_response = true;
                    in_BalenceError = false;
                }

                if (jsonObject.has(UserRegisterationActivity.error)) {

                    JSONObject errorObj = jsonObject.getJSONObject(UserRegisterationActivity.error);
                //    Log.d(HomeActivity.LogTag, "\n  Activity.error. : " + UserRegisterationActivity.error);

                    if(errorObj.has(UserRegisterationActivity.WALLET_BALANCE)) {
                //        Log.d(HomeActivity.LogTag, "\n  Activity.WALLET_BALANCE. : " + UserRegisterationActivity.WALLET_BALANCE);


                        EarnPointsActivity.walletBalance = errorObj.optString(UserRegisterationActivity.WALLET_BALANCE);
                        EarnPointsActivity.total_point = EarnPointsActivity.walletBalance;
                        SharedPreferences.Editor editor = getSharedPreferences(UserRegisterationActivity.MY_PREFS_NAME, MODE_PRIVATE).edit();
                        editor.putString(UserRegisterationActivity.total_point, EarnPointsActivity.total_point);
                        editor.commit();
                //        Log.d(HomeActivity.LogTag, "\n  Activity.WALLET_BALANCE. : " + PromoCodeActivity.total_point);

                        in_BalenceError = true;
                    }
                    if (errorObj.has(EarnPointsActivity.errorCode)) {
                        EarnPointsActivity.errorValue = errorObj.optString(EarnPointsActivity.errorCode);
                //        Log.d(HomeActivity.LogTag, "\n  errorValue. : " + PromoCodeActivity.errorValue);
                        EarnPointsActivity.errorMessageValue = errorObj.optString(EarnPointsActivity.errorMessage);
                //        Log.d(HomeActivity.LogTag, "\n  errorMessageValue. : " + PromoCodeActivity.errorMessageValue);
                        error_response = true;
                    }
                }
            } catch (JSONException error_302) {
                error_302.printStackTrace();
                Log.e(HomeActivity.LogTag, "\n  error_302. : " + error_302);
                parsing_error = true;
            }
        }


        @Override
        protected void onPostExecute(String data) {

            if (pd != null){
                if(storyIDOnServer || error_response || Utility.connection_error || parsing_error  || playStory){
            //        Log.d(HomeActivity.LogTag, "in pd" + pd);
                    pd.dismiss();
                }

            }

            EarnPointsActivity.myMsg = new TextView(SelectedStoryActivity.this);
            EarnPointsActivity.myMsg.setGravity(Gravity.CENTER_HORIZONTAL);
            EarnPointsActivity.myMsg.setTextSize(20);

            if(in_isPurchasedStory) {
                if (success_response) {
                    if(storyIDOnServer){

                         playStory();
                    }
                    EarnPointsActivity.myMsg.setText(EarnPointsActivity.congratulations);
                    EarnPointsActivity.success_msg = " Congratulations Your Wallet Balence is" + EarnPointsActivity.total_point;
                    String final_msg = EarnPointsActivity.success_msg;
                    EarnPointsActivity.finalMsg = final_msg;
                    if(!storyIDOnServer){
//                        showNewStoryAlertDialog();
                        syncUpdate = true;
                        updateBalance();
                    }

                } else if (error_response) {

                    EarnPointsActivity.myMsg.setText(EarnPointsActivity.errorValue);
                    EarnPointsActivity.finalMsg = EarnPointsActivity.errorMessageValue;

                } else if (Utility.connection_error) {

                    EarnPointsActivity.myMsg.setText("GS Error - C3");
                    EarnPointsActivity.finalMsg = UserRegisterationActivity.gss_error301;
                } else if (parsing_error) {
                    // set title
                    EarnPointsActivity.myMsg.setText("GS Error - P3");
                    EarnPointsActivity.finalMsg = UserRegisterationActivity.gss_error301;
                }
            }


            if(in_BalanceUpdate) {
                if (success_response) {

                    EarnPointsActivity.myMsg.setText(EarnPointsActivity.congratulations);

                    EarnPointsActivity.success_msg = " Congratulations Your Wallet Balence is" + EarnPointsActivity.total_point;
                    String final_msg = EarnPointsActivity.success_msg;
                    EarnPointsActivity.finalMsg = final_msg;
                    if(!buyStory) {
                        if (pd != null) {
                            pd.dismiss();
                        }
                        checkWalletBalance();
                    }
                    if(buyStory) {
                        PurchaseStory();
                    }
              /*      if (purchasedStoryListArr != null){
                        purchasedStoryListArr = loadArray(purchasedStoryListName,  context);
                        Log.d(HomeActivity.LogTag, "purchasedStoryListArr in checkSelectedStoryAccess " + purchasedStoryListArr);
                        if(purchasedStoryListArr.contains(storyID))
                        {
                            playStory();
                        }
                        else if(!storyIDOnServer){
                            //          if (success_response)
                            Log.d(HomeActivity.LogTag, "storyIDOnServer 3" + storyIDOnServer);
                            Log.d(HomeActivity.LogTag, "purchasedStoryListArr in checkSelectedStoryAccess in stoty id check " + storyIDOnServer);
                            HashMap<String, String> details = new HashMap<String, String>();
                            details.put(Utility.TAG_EMAIL_ID, PromoCodeActivity.email_ID);
                            details.put(Utility.StoryID, storyID);
                            loadAsAsyncTask(details, Utility.isPurchasedStoryURL);
                            Log.d(HomeActivity.LogTag, "storyIDOnServer 2" + storyIDOnServer);
                        }
                    }*/


                } else if (error_response) {

                    EarnPointsActivity.myMsg.setText(EarnPointsActivity.errorValue);
                    EarnPointsActivity.finalMsg = EarnPointsActivity.errorMessageValue;

                } else if (Utility.connection_error) {

                    EarnPointsActivity.myMsg.setText("GS Error - C3");
                    EarnPointsActivity.finalMsg = UserRegisterationActivity.gss_error301;
                } else if (parsing_error) {
                    // set title
                    EarnPointsActivity.myMsg.setText("GS Error - P3");
                    EarnPointsActivity.finalMsg = UserRegisterationActivity.gss_error301;
                }
            }


            if(purchasedStory) {
                if (success_response) {

                    EarnPointsActivity.myMsg.setText(EarnPointsActivity.congratulations);
                    EarnPointsActivity.success_msg = " Congratulations Your Wallet Balence is" + EarnPointsActivity.total_point;
                    String final_msg = EarnPointsActivity.success_msg;
                    EarnPointsActivity.finalMsg = final_msg;

                //    Log.d(HomeActivity.LogTag, "finally purchasedStoryListArr in post execute " + purchasedStoryListArr);
                    purchasedStoryListArr = loadStoryList(purchasedStoryListName, context);
                //    Log.d(HomeActivity.LogTag, "finally purchasedStoryListArr in post execute fromload stoty array " + purchasedStoryListArr);
                    showStoryPurchaseDialog();

                }
                else if (error_response)                {

                    EarnPointsActivity.myMsg.setText(EarnPointsActivity.errorValue);
                    EarnPointsActivity.finalMsg = EarnPointsActivity.errorMessageValue;
                }
                else if (Utility.connection_error)
                {
                    EarnPointsActivity.myMsg.setText("GS Error - C3");
                    EarnPointsActivity.finalMsg = UserRegisterationActivity.gss_error301;
                }
                else if (parsing_error) {
                    // set title
                    EarnPointsActivity.myMsg.setText("GS Error - P3");
                    EarnPointsActivity.finalMsg = UserRegisterationActivity.gss_error301;
                }
            }


            if(in_BalenceError){
            //    Log.d(HomeActivity.LogTag, "in update" + pd);

                updateBalance();
            }
//            if(!oneTimeCheck) {
//                oneTimeCheck = true;
//                checkStory();
//            }
            else if(parsing_error || Utility.connection_error || error_response){
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder.setCustomTitle(EarnPointsActivity.myMsg);
                // set dialog message
                alertDialogBuilder.setMessage(EarnPointsActivity.finalMsg);
                alertDialogBuilder
                        .setCancelable(false)
                        .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // if this button is clicked, just close
                                // the dialog box and do nothing
                                dialog.cancel();
                            }
                        });
                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();
                // show it
                alertDialog.show();
            }

        }
    }

    private void PurchaseStory() {
        HashMap<String, String> details = new HashMap<String, String>();
        details.put(Utility.TAG_EMAIL_ID, EarnPointsActivity.email_ID);
        details.put(Utility.StoryID, storyID);
        loadAsAsyncTask(details, Utility.purchaseStoryURL);
    }

  /*  private void checkStory() {
        if (purchasedStoryListArr != null){
            purchasedStoryListArr = loadArray(purchasedStoryListName,  context);
            Log.d(HomeActivity.LogTag, "purchasedStoryListArr in checkSelectedStoryAccess " + purchasedStoryListArr);
            if(purchasedStoryListArr.contains(storyID))
            {
                playStory();
            }
            else if(!storyIDOnServer){
                //          if (success_response)
                Log.d(HomeActivity.LogTag, "storyIDOnServer 3" + storyIDOnServer);
                Log.d(HomeActivity.LogTag, "purchasedStoryListArr in checkSelectedStoryAccess in stoty id check " + storyIDOnServer);
                HashMap<String, String> details = new HashMap<String, String>();
                details.put(Utility.TAG_EMAIL_ID, PromoCodeActivity.email_ID);
                details.put(Utility.StoryID, storyID);
                loadAsAsyncTask(details, Utility.isPurchasedStoryURL);
                Log.d(HomeActivity.LogTag, "storyIDOnServer 2" + storyIDOnServer);
                if(storyIDOnServer){
                    Log.d(HomeActivity.LogTag, "storyIDOnServer 1" + storyIDOnServer);
                    playStory();
                    Log.d(HomeActivity.LogTag, "storyIDOnServer " + storyIDOnServer);
                }

            }else {
                showNewStoryAlertDialog();
            }
        }

    }*/

    // public void startAudioStreaming(View v){ audioPlayerViewObj.streamAudioPlayer();}

  //  @Override
  //  public void onClick(View v) {
  //       startAudioStreaming(v);
  //  }
}

