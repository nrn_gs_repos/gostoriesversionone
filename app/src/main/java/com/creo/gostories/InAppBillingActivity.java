package com.creo.gostories;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.creo.gostories.util.IabHelper;
import com.creo.gostories.util.IabResult;
import com.creo.gostories.util.Inventory;
import com.creo.gostories.util.Purchase;

import java.util.ArrayList;


public class InAppBillingActivity extends Activity {

    public static final String POINTS_PACK_500 = "com.nishant.learn.nishiap.p500";
    public static final String POINTS_PACK_200 = "com.nishant.learn.nishiap.p200";
    static final int RC_REQUEST = 10001;
    String developerPayload = "NishantDeveloperPayload";
    IabHelper mHelper;
    public final static String TAG = "InAppBilling_LOG_TAG";
    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener;
    IabHelper.QueryInventoryFinishedListener mQueryFinishedListener;
    ArrayList<String> additionalSkuList;
    public ListView skuItemsListView;
    ArrayAdapter<String>  iapListArrayAdapter;
    public ArrayList<String> customIAPArrList = new ArrayList<>();
    ProgressDialog progressDialog;
    Toast mToast;
    public int purchasedPointsBasket = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.in_app_billing_layout);

        customIAPArrList.add("Nishant");
        skuItemsListView = (ListView) findViewById(R.id.skuListView);
        iapListArrayAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, customIAPArrList);

        skuItemsListView.setVisibility(View.INVISIBLE);
        skuItemsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                launchPurchaseFlowForSelectedItem(POINTS_PACK_200);
            }
        });

//        String appVersionNameStr = ""+BuildConfig.VERSION_CODE;
//        showToastWithMessage("appVersionNameStr: "+appVersionNameStr);
        // Assign adapter to ListView
        skuItemsListView.setAdapter(iapListArrayAdapter);

        String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAi/EUBNGNJqn5QT1svxs/uWI4aTN9yj721VMD5DFukjJk+WH4q3qhSta9PP1hHL2aqieCxiO7nhue0cGcWvZoU5eIsaKGLStzBsGqxTIhg4E+GuYSrDjG8l7gecR5CyiF9fQAth33h+WNa5dboEZi2Zys01GKkXPXrdLRadFvoGLs4/yYRYrUKBFrU2MJtF4uibzm1vv3Gn8JjfZI8s+MPrXKA1Q07wnlDJgkmyvzvP/utBzPmK+I76hQO3U8QDBU8JUXy5+c+QkVZR/l/uXU8xWUEXHS8iXG7yIeo/Syt6G2VSOQa6rAdGov/Los3QM5PcWoV1KmxAzamqLbuHPaGwIDAQAB";
        mHelper = new IabHelper(this, base64EncodedPublicKey);
        mHelper.enableDebugLogging(true);

        additionalSkuList = new ArrayList<>();
        additionalSkuList.add(POINTS_PACK_500);
        additionalSkuList.add(POINTS_PACK_200);

        startSetupOfIABHelper();
        setupQueryFinishedListener();
        setupPurchaseFinishedListener();
    }

    public void startSetupOfIABHelper(){
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                // Log.d(TAG, "Setup finished.");

                if (!result.isSuccess()) {
                    // Oh noes, there was a problem.
                    String errMsg = "-- Basic setup of IAB helper Failed. "+result.getMessage();
                    Log.e(TAG, errMsg);
                    showToastWithMessage(errMsg);
                    return;
                }

                // Have we been disposed of in the meantime? If so, quit.
                if (mHelper == null) return;

                // IAB is fully set up. Now, let's get an inventory of stuff we own.
                //Log.d(TAG, "IabHelper Setup successful..");
                //launchPurchaseFlowForSelectedItem(POINTS_PACK_200);
            }
        });
    }

    public void setupQueryFinishedListener(){
        mQueryFinishedListener = new IabHelper.QueryInventoryFinishedListener() {
            public void onQueryInventoryFinished(IabResult result, Inventory inventory)
            {
                if (progressDialog != null){
                    progressDialog.dismiss();
                }
                Log.d(TAG, "onQueryInventoryFinished...   inventory: "+inventory );
                if (result.isFailure()) {
                    String errMsg = "-- Problem setting up In-app Billing. "+result.getMessage();
                    Log.e(TAG, errMsg);
                    showToastWithMessage(errMsg);
                    return;
                }

                try
                {
                    Log.d(TAG, "inventory: " + inventory);
                    Log.d(TAG, "skuItemsListView: "+ skuItemsListView);
                    Log.d(TAG, "iapListArrayAdapter: "+iapListArrayAdapter);
                    Log.d(TAG, "customIAPArrList: "+customIAPArrList);

                    customIAPArrList.clear();

                    String POINTS_PACK_200_Price = "POINTS_PACK_200_Price not received";
                    if (inventory.getSkuDetails(POINTS_PACK_200) != null){
                        POINTS_PACK_200_Price = inventory.getSkuDetails(POINTS_PACK_200).getPrice();
                        String pack200Title = inventory.getSkuDetails(POINTS_PACK_200).getTitle();
                        if (pack200Title != null && POINTS_PACK_200_Price != null){
                            POINTS_PACK_200_Price = pack200Title + "  -  " + POINTS_PACK_200_Price;
                        }
                    }
                    customIAPArrList.add(POINTS_PACK_200_Price);

                    String POINTS_PACK_500_Price = "POINTS_PACK_200_Price not received";
                    if (inventory.getSkuDetails(POINTS_PACK_200) != null){
                        POINTS_PACK_500_Price = inventory.getSkuDetails(POINTS_PACK_500).getPrice();
                        String pack500Title = inventory.getSkuDetails(POINTS_PACK_500).getTitle();
                        if (POINTS_PACK_500_Price != null && POINTS_PACK_200_Price != null){
                            POINTS_PACK_500_Price = pack500Title + "  -  " + POINTS_PACK_200_Price;
                        }
                    }
                    customIAPArrList.add( POINTS_PACK_500_Price);

                    // Log.d(TAG, "customIAPArrList: " + customIAPArrList);
                    skuItemsListView.setVisibility(View.VISIBLE);
                    iapListArrayAdapter.notifyDataSetChanged();

                    // update the UI
                }
                catch (Exception e){
                    e.printStackTrace();
                    String errMsg = "-- onQueryInventoryFinished Failed. "+e.getLocalizedMessage();
                    Log.e(TAG, errMsg);
                    showToastWithMessage(errMsg);
                }
            }
        };
    }

    public void setupPurchaseFinishedListener(){
        // Callback for when a purchase is finished
        mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
            public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
                //Log.d(TAG, "Purchase finished: " + result + ", purchase: " + purchase);

                // if we were disposed of in the meantime, quit.
                if (mHelper == null) return;

                if (result.isFailure()) {
                    String errMsg = "-- Purchase failed. "+result.getMessage();
                    Log.e(TAG, errMsg);
                    showToastWithMessage(errMsg);
                    return;
                }
                showToastWithMessage(purchase.getSku());
                purchasedPointsBasket = purchasedPointsBasket + 9999;
            }
        };
    }

    public void showToastWithMessage(String msg) {
        mToast = new Toast(InAppBillingActivity.this);
        if (mToast == null){
            mToast = new Toast(InAppBillingActivity.this);
        }else{
            mToast.cancel();
        }
        mToast.makeText(getApplicationContext(),
                msg, Toast.LENGTH_LONG)
                .show();
    }

    public void queryAsync(){
        try{
            mHelper.queryInventoryAsync(true, additionalSkuList,
                    mQueryFinishedListener);
        }catch (Exception e){
            String errMsg = "-- Failed to query Inventory. "+e.getLocalizedMessage();
            Log.e(TAG, errMsg);
            showToastWithMessage(errMsg);

            if (progressDialog != null){
                progressDialog.dismiss();
            }
        }
    }

    public void launchPurchaseFlowForSelectedItem(String selectedItemId){
        try {
            if (mHelper.mAsyncInProgress){mHelper.flagEndAsync();}
            mHelper.launchPurchaseFlow(InAppBillingActivity.this, selectedItemId, RC_REQUEST, mPurchaseFinishedListener, developerPayload);
        }catch (Exception e){
            String errMsg = "-- Failed to start Purchase for: "+selectedItemId+"    "+e.getLocalizedMessage();
            Log.e(TAG, errMsg);
            showToastWithMessage(errMsg);
            if (progressDialog != null){
                progressDialog.dismiss();
            }
        }
    }

    public void loadIAPItems(View v){
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading IAP items..");
        progressDialog.setCancelable(false);
        progressDialog.show();
        queryAsync();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mHelper != null) mHelper.dispose();
        mHelper = null;
        purchasedPointsBasket = 0;
    }
}
